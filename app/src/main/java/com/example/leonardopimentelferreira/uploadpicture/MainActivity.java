package com.example.leonardopimentelferreira.uploadpicture;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends ActionBarActivity {
    static final int REQUEST_TAKE_PHOTO = 1;
    private ImageView imgPictureTaken;
    private Button btCamera;
    private Button btUpload;
    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("onCreate","");
        btCamera = (Button) findViewById(R.id.bt_take_picture);
        btUpload = (Button) findViewById(R.id.bt_upload);
        imgPictureTaken = (ImageView)findViewById(R.id.img_view_taken_picture);

        // Check whether we're recreating a previously destroyed instance
//        if (savedInstanceState != null) {
//            // Restore value of members from saved state
//            mCurrentPhotoPath = savedInstanceState.getString("CUR_PHOTO_PATH");
//            if(mCurrentPhotoPath != null && fileExists(mCurrentPhotoPath)){
//                Log.e("CUR_PHOTO_PATH", mCurrentPhotoPath);
//                imgPictureTaken.setImageBitmap(decodeSampledBitmapFromResource(mCurrentPhotoPath, 100, 100, true));
//            }
//        } else {
//            // Probably initialize members with default values for a new instance
//        }

        btCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        btUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryAddPic(mCurrentPhotoPath);
                //dispatchTakePictureIntent();
                if(mCurrentPhotoPath != null) {
                    ConnectivityManager connMgr = (ConnectivityManager)
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        new UploadImageTask().execute(mCurrentPhotoPath);
                    } else {
                        Toast.makeText(MainActivity.this, "No network connection available.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private boolean fileExists(String filePath) {
        File file = new File(filePath);
        if(file.exists())
            return true;
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Create an image file in a format JPEG_yyyyMMdd_HHmmss_NUMBER.jpg and store it in the external directory for pictures
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Starts an activity to take picture
     */
    private void dispatchTakePictureIntent() {
        //check if the device has a cam
        boolean hasCamera = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if(hasCamera){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            //if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }else{
                Log.e("Error createImageFile","");
            }
            //}
        }else{
            Toast.makeText(this, "Sorry, this device has no camera.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Add a picture to the gallery
     * @param uri
     */
    private void galleryAddPic(String uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Log.e("URI",uri);
        File f = new File(uri);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //after dispatchTakePictureIntent()
        //The Android Camera application encodes the photo in the return Intent delivered to onActivityResult() as a small Bitmap in the extras, under the key "data".
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            try {
//                galleryAddPic(mCurrentPhotoPath);
                imgPictureTaken.setImageBitmap(decodeSampledBitmapFromResource(mCurrentPhotoPath, 100, 100, true));
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(this, "Sorry, we could not load the image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Calculates a sample size of the image
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Decode a sampled bitmap
     * @param uri
     * @param reqWidth
     * @param reqHeight
     * @param rotation
     * @return
     */
    public static Bitmap decodeSampledBitmapFromResource(String uri,
                                                         int reqWidth,
                                                         int reqHeight,
                                                         boolean rotation) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(uri, options);

        if(rotation) {
            try {
                ExifInterface exif = new ExifInterface(uri);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                bitmap = rotateBitmap(bitmap, orientation);
                Log.e("Orientation ", String.valueOf(orientation));
            }catch (IOException e){
                e.printStackTrace();
                return bitmap;
            }
        }
        return bitmap;
    }

//    private Bitmap setPic(String uri) throws IOException {
//        ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
//        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//        Log.e("Orientation ",String.valueOf(orientation));
//
//        Bitmap bitmap = BitmapFactory.decodeFile(uri, null);
//        bitmap = rotateBitmap(bitmap, orientation);
//        return bitmap;
//    }

    /**
     * Rotate a bitmap in order to be user-friendly
     * @param bitmap
     * @param orientation
     * @return
     * @throws IOException
     */
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) throws IOException {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Created by leonardopimentelferreira on 3/8/15.
     * Creates a Thread to upload images to the server
     */
    private class UploadImageTask extends AsyncTask<String, Void, String> {
        public String username="";

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                Log.e("doInBackground", "function");
                output = uploadFile(url);
            }
            return output;
        }

        public String uploadFile(String sourceFileUri) {
            int serverResponseCode = 0;
            String fileName = sourceFileUri;
            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(sourceFileUri);

            if (sourceFile.isFile()) {
                try {
                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(Config.SERVER_PHOTOS);

                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy

//                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(5000 /* milliseconds */);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("uploaded_file", fileName);

                    dos = new DataOutputStream(conn.getOutputStream());

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                            + fileName + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    // create a buffer of  maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();

                    Log.i("uploadFile", "HTTP Response is : "
                            + serverResponseMessage + ": " + serverResponseCode);

                    if(serverResponseCode == HttpURLConnection.HTTP_OK){
                        //Get Response
                        InputStream is = conn.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                        String line;
                        StringBuffer response = new StringBuffer();
                        while ((line = rd.readLine()) != null) {
                            response.append(line);
                            response.append('\r');
                        }
                        rd.close();
                        Log.i("uploadFile", response.toString());

                        JSONObject jObject = new JSONObject(response.toString());
                        Integer upload_ok = jObject.getInt("upload_ok");
                        final String message = jObject.getString("message");
                        Log.i("uploadFile", upload_ok + " " + message);
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                                    imgPictureTaken.setImageBitmap(null);
                                    mCurrentPhotoPath = null;

                                }
                            });
                    }else{
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.this, "Sorry, we could not upload the image. Try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    //close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                    Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "Sorry, we could not upload the image. Try again!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (SocketTimeoutException ex){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "Sorry, we could not connect to the server. Try again!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Upload file:", e.getMessage());
                }
                return "";

            }else { // End else block
                Log.e("It is not a file", ":("+sourceFileUri);
            }
            return "";
        }

        // Reads an InputStream and converts it to a String.
        public String readResponseFromServer(InputStream stream) throws IOException, UnsupportedEncodingException {
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            return sb.toString();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //save the current path in case that
        savedInstanceState.putString("CUR_PHOTO_PATH", mCurrentPhotoPath);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore state members from saved instance
        mCurrentPhotoPath = savedInstanceState.getString("CUR_PHOTO_PATH");
        if(mCurrentPhotoPath != null && fileExists(mCurrentPhotoPath)){
            Log.e("CUR_PHOTO_PATH", mCurrentPhotoPath);
            imgPictureTaken.setImageBitmap(decodeSampledBitmapFromResource(mCurrentPhotoPath, 100, 100, true));
        }
    }
}